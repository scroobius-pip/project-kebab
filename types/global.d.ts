declare type tradeType = 'sale' | 'swap'
declare type statusType = 'has' | 'want'

interface GAME_TYPE {
  name: string
  consoleType: string
  imageUrl?: string
  id: string
}

interface OFFER_SUBMISSION {
  meId: string
  otherId: string
  meSelectedGames: USER_GAME[]
  otherSelectedGames: USER_GAME[]
}

interface USER_TYPE {
  id: string
  userName: string
  userImageUrl: string
  isPro: boolean
  noOfSuccessfulExchanges: number
  joinedDate: {
    month: string
    year: string
  }
  location: { city: string; state: string }
  rating: {
    positive: number
    negative: number
  }
  description: string
}

interface USER_GAME extends GAME_TYPE {
  description: string
  tradeType: tradeType
  statusType?: statusType
}

interface USER_WITH_GAMES {
  info: USER_TYPE
  games: USER_GAME[]
}

interface SEARCH_RESULT {
  [consoleType: string]: GAME_TYPE[]
}

interface FORM_FIELD_GAME_DETAILS {
  description?: string
  tradeType?: tradeType
}

interface MATCH_TYPE {
  me: USER_WITH_GAMES
  other: USER_WITH_GAMES
  matchRate: number
  distance: number
  type: 'matchRate' | 'distance'
}

interface OFFER_TYPE {
  me: USER_WITH_GAMES
  other: USER_WITH_GAMES
  offerId: string
  status: 'pending' | 'ongoing' | 'completed' | 'cancelled'
}

