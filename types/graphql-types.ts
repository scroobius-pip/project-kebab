/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export interface SearchGamesQueryInput {
  searchText: string;
  limit?: number | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
