import { Query } from 'react-apollo';
import { Games, GamesVariables } from 'queries/types/Games';

export class SearchGamesQuery extends Query<Games, GamesVariables>{ }
