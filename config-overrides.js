const { getLoader, injectBabelPlugin } = require('react-app-rewired')
const tsImportPluginFactory = require('ts-import-plugin')
const rewireReactHotLoader = require('react-app-rewire-hot-loader')
const rewireTypescript = require('react-app-rewire-typescript')

module.exports = function override(config, env) {
  // do stuff with the webpack config...
  config = rewireTypescript(config, env)
  config = rewireReactHotLoader(config, env)

  const tsLoader = getLoader(
    config.module.rules,
    rule => rule.loader && typeof rule.loader === 'string' && rule.loader.includes('ts-loader')
  )

  tsLoader.options = {
    getCustomTransformers: () => ({
      before: [
        tsImportPluginFactory([
          {
            libraryDirectory: 'es',
            libraryName: 'antd',
            style: 'css'
          }
        ])
      ]
    })
  }

  return config
}
