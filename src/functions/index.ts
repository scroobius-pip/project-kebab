import generateColorFromString from './generateColorFromString'
import searchGames from './Graphql/searchGames'
import urlToList from './urlToList'
export { generateColorFromString, searchGames, urlToList }
