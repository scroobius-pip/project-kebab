
const loginToken = {
    get: () => {
        return localStorage.getItem('token')
    },
    set: (value: string) => {
        localStorage.setItem('token', value)
    },
    delete: () => {
        localStorage.removeItem('token')
    }
}

export default loginToken