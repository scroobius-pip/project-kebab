import apolloGraphql from '../../apolloGraphql'
import getUserDescriptionQuery from 'queries/getUserDescriptionQuery'
import { Description } from 'queries/types/Description'

export default async (): Promise<string> => {
    const results = await apolloGraphql.query<Description>({ query: getUserDescriptionQuery })
    if (!results.data.me) { throw new Error('Unable To Retrieve') }
    return results.data.me.info.description || ''
}