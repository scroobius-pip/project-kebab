import apolloGraphql from '../../apolloGraphql'
import searchGames from 'queries/searchGamesQuery';
import { Games, GamesVariables, Games_searchGames_result } from 'queries/types/Games';


const groupGamesByConsoleType = (games: GAME_TYPE[]): SEARCH_RESULT => {
  return games.reduce((r, a) => {
    r[a.consoleType] = r[a.consoleType] || []
    r[a.consoleType].push(a)
    return r
  }, Object.create(null))
}

const mapResultToGameType = (game: Games_searchGames_result): GAME_TYPE => {
  return {
    consoleType: game.consoleType || '',
    id: game.id || '',
    imageUrl: game.imageUrl || '',
    name: game.name || ''
  }
}

export default async (searchString: string): Promise<SEARCH_RESULT> => {

  const results = await apolloGraphql.query<Games, GamesVariables>({ query: searchGames, variables: { input: { limit: 10, searchText: searchString } } })
  const games = results.data.searchGames.result || []

  const gameResults = groupGamesByConsoleType(games.map(mapResultToGameType))
  return gameResults

}
