import apolloGraphql from '../../apolloGraphql'
import addGamesMutation from 'mutations/addGamesMutationAndUpdateDescriptionMutation';
import { addGamesAndUpdateDescription, addGamesAndUpdateDescriptionVariables } from 'mutations/types/addGamesAndUpdateDescription'
import { AddGamesInput, UserGameDetailsStatus, UserGameDetailsTradeType } from 'types/graphql-types';

interface MutationResult { success: boolean, error?: string }


const mapUserGameToAddGameInput = (game: USER_GAME): AddGamesInput => {

    return {
        details: {
            description: game.description,
            status: UserGameDetailsStatus[game.statusType || 'has'],
            tradeType: UserGameDetailsTradeType[game.tradeType],
        },
        gameId: game.id
    }

}


export default async (games: USER_GAME[], description: string): Promise<MutationResult> => {
    try {
        const parsedGames = games.map(mapUserGameToAddGameInput)
        const result = await apolloGraphql.mutate<addGamesAndUpdateDescription, addGamesAndUpdateDescriptionVariables>({ mutation: addGamesMutation, variables: { games: parsedGames, description } })
        if (result.data && result.data.addUserGames && result.data.addUserGames.error) {
            return { error: result.data.addUserGames.error.message, success: false }
        }
        else {
            return { success: true }
        }

    } catch (error) {
        console.error(error)
        return { error: error.message, success: false }
    }
}