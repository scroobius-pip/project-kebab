import apolloGraphql from '../../apolloGraphql'
import getUserGamesHasQuery from 'queries/getUserGamesHasQuery'
import { UserGame, UserGame_me_wantedGames, } from 'queries/types/UserGame'

const mapGameTypeToUserGame = (gameResult: UserGame_me_wantedGames): USER_GAME => {

    const { details: { description, tradeType, status }, game: { consoleType, imageUrl, name }, id } = gameResult
    return {
        consoleType: consoleType || '',
        description,
        statusType: status,
        id,
        imageUrl: imageUrl || '',
        name,
        tradeType

    }
}

export default async (): Promise<USER_GAME[]> => {

    const results = await apolloGraphql.query<UserGame>({ query: getUserGamesHasQuery })

    if (!results.data.me) { throw new Error('Unable To Retrieve') }
    // @ts-ignore
    return (results.data.me.hasGames || []).map(mapGameTypeToUserGame) // issue with graphql code generator
}