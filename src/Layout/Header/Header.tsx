import React from 'react'
import { Layout, Menu, Icon } from 'antd'
import ProfileImage from '../../components/ProfileImage'
// import SearchBox from '../SearchBox'
// import SearchGames from '../../functions/searchGames'
// import './TopBar.css'
import { Link } from 'react-router-dom'
const { Header } = Layout

// const initSearch = new SearchGames()

export default () => {
  return (
    <Header style={{ display: 'flex', justifyContent: 'center' }}>
      <div>
        <div style={{ display: 'inline-block', height: 64 }}>
          <Link to="/">
            <img style={{ height: 32 }} src={require('../../assets/logo.svg')} />
            <h1
              style={{
                color: 'white',
                display: 'inline-block',
                fontSize: 16,
                verticalAlign: 'middle',
                marginLeft: 10,
                marginBottom: 0
              }}>
              Project Kebab
            </h1>
          </Link>
        </div>

        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['2']}
          style={{ display: 'inline-block', marginLeft: 40 }}>
          <Menu.Item key="matches">
            <Link to="matches">
              <Icon type="swap" />
              Matches
            </Link>
          </Menu.Item>
          <Menu.Item key="offers">
            <Link to="offers">
              <Icon type="swap-right" />
              Offers
            </Link>
          </Menu.Item>

          <Menu.Item style={{ marginLeft: 100 }} key="mygames">
            <Link to="mygames">
              <Icon type="appstore" />
              My Games
            </Link>
          </Menu.Item>
          <Menu.Item>
            <ProfileImage
              id="simd"
              isPro={true}
              joinedDate={{ month: 'December', year: '2015' }}
              noOfSuccessfulExchanges={2}
              rating={{ negative: 0, positive: 5 }}
              userImageUrl="https://s.gravatar.com/avatar/45a59387d15d83e94c65e12b483c8b00?size=496&default=retro"
              size={40}
            />
            <span style={{ marginLeft: 10 }}>sim04ful</span>
          </Menu.Item>
        </Menu>
      </div>
    </Header>
  )
}
