import React from 'react'
import { Layout, Divider } from 'antd'

const { Footer } = Layout
// const initSearch = new SearchGames()

export default () => {
  return (
    <Footer>
      Kebab Project ©2018
      <Divider type="vertical" />
    </Footer>
  )
}
