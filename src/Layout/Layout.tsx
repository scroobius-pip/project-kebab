import React from 'react'
import Content from './Content'
import Header from './Header'
import Footer from './Footer'
import { Layout } from 'antd'
export default ({ children }: { children: JSX.Element }) => {
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Header />
      <Content>{children}</Content>
      <Footer />
    </Layout>
  )
}
