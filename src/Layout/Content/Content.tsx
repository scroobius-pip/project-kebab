import React from 'react'
import { Layout } from 'antd'

const { Content } = Layout

export default ({ children }: { children: JSX.Element }) => (
  <Content style={{ maxWidth: 800, margin: 'auto' }}>{children}</Content>
)
