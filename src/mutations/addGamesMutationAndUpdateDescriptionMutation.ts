import { gql } from 'apollo-boost'

export default gql`
mutation addGamesAndUpdateDescription ($games:[AddGamesInput!]!,$description:String!){
    addUserGames(input: {games:$games}) {
        result
        error {
            message
            type
            id
        }
    }
    updateUserInfo(input: {info:{description:$description}}) {
        result {
            description
        }
        error {
            message
            type
            id
        }
    }
}`