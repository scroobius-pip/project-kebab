/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { AddGamesInput } from "./../../types/graphql-types";

// ====================================================
// GraphQL mutation operation: addGamesAndUpdateDescription
// ====================================================

export interface addGamesAndUpdateDescription_addUserGames_error {
  __typename: "Error";
  message: string;
  type: string;
  id: string;
}

export interface addGamesAndUpdateDescription_addUserGames {
  __typename: "AddGamesMutationResult";
  result: boolean;
  error: addGamesAndUpdateDescription_addUserGames_error | null;
}

export interface addGamesAndUpdateDescription_updateUserInfo_result {
  __typename: "UserInfo";
  description: string | null;
}

export interface addGamesAndUpdateDescription_updateUserInfo_error {
  __typename: "Error";
  message: string;
  type: string;
  id: string;
}

export interface addGamesAndUpdateDescription_updateUserInfo {
  __typename: "UpdateUserInfoMutationResult";
  result: addGamesAndUpdateDescription_updateUserInfo_result | null;
  error: addGamesAndUpdateDescription_updateUserInfo_error | null;
}

export interface addGamesAndUpdateDescription {
  addUserGames: addGamesAndUpdateDescription_addUserGames | null;
  updateUserInfo: addGamesAndUpdateDescription_updateUserInfo | null;
}

export interface addGamesAndUpdateDescriptionVariables {
  games: AddGamesInput[];
  description: string;
}
