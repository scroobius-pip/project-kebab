import { RouteProps } from 'react-router-dom'
import { MatchesSection, OffersSection, AddGameSection } from './sections'

const routes: RouteProps[] = [
  {
    path: '/offers',
    component: OffersSection
  },
  {
    path: '/matches',
    component: MatchesSection
  },
  {
    path: '/mygames',
    component: AddGameSection,
  }
]

export default routes
