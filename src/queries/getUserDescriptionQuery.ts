import { gql } from 'apollo-boost'
const getUserDescriptionQuery = gql`
query Description {
    me {
        info {
        description
    }
    }
}
`

export default getUserDescriptionQuery