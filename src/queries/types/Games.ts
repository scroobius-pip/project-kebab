/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { SearchGamesQueryInput } from "./../../types/graphql-types";

// ====================================================
// GraphQL query operation: Games
// ====================================================

export interface Games_searchGames_result {
  __typename: "Game";
  consoleType: string | null;
  id: string;
  imageUrl: string | null;
  name: string;
}

export interface Games_searchGames {
  __typename: "SearchGamesQueryResult";
  result: Games_searchGames_result[] | null;
}

export interface Games {
  searchGames: Games_searchGames;
}

export interface GamesVariables {
  input: SearchGamesQueryInput;
}
