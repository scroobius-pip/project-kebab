/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Description
// ====================================================

export interface Description_me_info {
  __typename: "UserInfo";
  description: string | null;
}

export interface Description_me {
  __typename: "User";
  info: Description_me_info;
}

export interface Description {
  me: Description_me | null;
}
