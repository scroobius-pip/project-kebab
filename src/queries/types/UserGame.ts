/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

import { UserGameDetailsStatus, UserGameDetailsTradeType } from "./../../types/graphql-types";

// ====================================================
// GraphQL query operation: UserGame
// ====================================================

export interface UserGame_me_wantedGames_details {
  __typename: "UserGameDetails";
  description: string;
  status: UserGameDetailsStatus;
  tradeType: UserGameDetailsTradeType;
}

export interface UserGame_me_wantedGames_game {
  __typename: "Game";
  consoleType: string | null;
  id: string;
  imageUrl: string | null;
  name: string;
}

export interface UserGame_me_wantedGames {
  __typename: "UserGame";
  details: UserGame_me_wantedGames_details;
  id: string;
  game: UserGame_me_wantedGames_game;
}

export interface UserGame_me {
  __typename: "User";
  wantedGames: UserGame_me_wantedGames[] | null;
}

export interface UserGame {
  me: UserGame_me | null;
}
