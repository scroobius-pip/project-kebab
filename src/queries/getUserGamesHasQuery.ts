import { gql } from 'apollo-boost'

const getUserGames = gql`
query UserGame {
    me {
        hasGames {
            details {
            description
            status
            tradeType
        }
        id
        game {
            consoleType
            id
            imageUrl
            name
        }
        }
    }
}`

export default getUserGames