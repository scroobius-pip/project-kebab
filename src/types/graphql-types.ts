/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum UserGameDetailsStatus {
  has = "has",
  want = "want",
}

export enum UserGameDetailsTradeType {
  sale = "sale",
  swap = "swap",
}

export interface AddGamesInput {
  gameId: string;
  details: UserGameDetailsInput;
}

export interface SearchGamesQueryInput {
  searchText: string;
  limit?: number | null;
}

export interface UserGameDetailsInput {
  description: string;
  status?: UserGameDetailsStatus | null;
  tradeType: UserGameDetailsTradeType;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
