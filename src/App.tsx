import { hot } from 'react-hot-loader'
import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Layout from './Layout'
import Routes from 'routes'
import { LoginCard } from './components'
import './App.css'
import loginToken from 'functions/loginToken'



class App extends React.Component {
  public render() {
    return (
      <Router>
        <div>
          {!loginToken.get() ? <LoginCard /> : null}
          <Layout>
            <Switch>
              {Routes.map(route => (
                <Route {...route} />
              ))}
            </Switch>
          </Layout>
        </div>
      </Router>
    )
  }
}

export default hot(module)(App)
// export default App
