import React, { Component } from 'react'
import { Row, List, message, Col } from 'antd'
import { SortMatchesSwitch, Container, MatchCard } from '../../components'
import mockMatches from './matchesData.mock'

interface State {
  matches: MATCH_TYPE[]
  loading: boolean
  matchType: MATCH_TYPE['type']
}

const submitSubmission = (submission: OFFER_SUBMISSION) => {
  message.info('Submitted ' + JSON.stringify(submission))
}

const MatchCardItem = (props: MATCH_TYPE) => (
  // @ts-ignore
  <List.Item key={props.other.info.id} xl={4}>
    <MatchCard onSubmit={submitSubmission} {...props} />
  </List.Item>
)

export default class extends Component<any, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      matches: [],
      loading: false,
      matchType: 'distance'
    }
  }

  public async componentDidMount() {
    await this.getAndRenderMatches()
  }

  private async getAndRenderMatches() {
    this.setLoading(true)
    const matches = await this.getMatches(this.state.matchType)
    this.setMatchesState(matches)
    this.setLoading(false)
  }

  public componentDidUpdate(prevProps: any, prevState: State) {
    if (prevState.matchType !== this.state.matchType) {
      this.getAndRenderMatches()
    }
  }

  private async getMatches(type: MATCH_TYPE['type']): Promise<MATCH_TYPE[]> {
    const comparisonFunction = (a: any, b: any) => b[type] - a[type]
    const matches = await mockMatches(1, type).sort(comparisonFunction)
    return matches
  }

  private setMatchesState(matches: MATCH_TYPE[]) {
    this.setState({
      matches
    })
  }

  private setLoading(loading: boolean) {
    this.setState({
      loading
    })
  }

  private setMatchType = (matchType: MATCH_TYPE['type']) => {
    this.setState({
      matchType
    })
  }

  public render() {
    const { matchType, loading, matches } = this.state
    return (
      <Container style={{ width: '100%', minHeight: '80vh' }}>
        <Row align="middle" >
          <Col style={{ display: 'flex', justifyContent: 'center' }} span={24}>
            <SortMatchesSwitch
              value={this.state.matchType}
              sortChange={this.setMatchType}
              style={{ marginBottom: 20 }}
            />
          </Col>
          <Col span={24}>
            <List
              header={
                <h1 style={{ textAlign: 'center', }}>
                  {matchType === 'matchRate' ? 'Similar Gamers' : 'Gamers Near You'}
                </h1>
              }
              pagination={{ position: 'bottom' }}
              loading={loading}
              grid={{ xs: 1, sm: 1, md: 1, xl: 1, gutter: 48 }}
              dataSource={matches}
              renderItem={MatchCardItem}
            />
          </Col>
        </Row>
      </Container>
    )
  }
}
