import faker from 'faker'

const consoleTypes = ['PlayStation', 'PlayStation 3', 'Xbox', 'PlayStation 4']
const tradeType: tradeType[] = ['swap', 'sale']
const statusType: statusType[] = ['has', 'want']

const UserGame = (): USER_GAME => ({
  name: (() => {
    const s = faker.lorem.words(faker.random.number(5) || 2)
    return s.charAt(0).toUpperCase() + s.slice(1)
  })(),
  consoleType: faker.random.arrayElement(consoleTypes),
  id: faker.random.uuid(),
  tradeType: faker.random.arrayElement(tradeType),
  description: faker.lorem.sentence(),
  statusType: faker.random.arrayElement(statusType)
})

const UserInfo = (): USER_TYPE => ({
  description: faker.lorem.paragraph(),
  id: faker.random.uuid(),
  isPro: faker.random.boolean(),
  joinedDate: {
    month: faker.date.month(),
    year: faker.date
      .between('2005', '2018')
      .getFullYear()
      .toString()
  },
  location: {
    city: faker.address.city(),
    state: faker.address.stateAbbr()
  },
  noOfSuccessfulExchanges: faker.random.number(100),
  rating: {
    negative: 0,
    positive: faker.random.number(100)
  },
  userImageUrl: faker.internet.avatar(),
  userName: faker.internet.userName()
})

const User = () => ({
  info: UserInfo(),
  games: (() => {
    const games = []
    const gameCount = faker.random.number(10) || 1
    for (let x = 0; x < gameCount; ++x) {
      games.push(UserGame())
    }
    return games
  })()
})

export default (
  amount: number,
  matchType: MATCH_TYPE['type']
): Array<{
  me: USER_WITH_GAMES
  other: USER_WITH_GAMES
  matchRate: number
  distance: number
  type: MATCH_TYPE['type']
}> => {
  const matchesList = []
  for (let x = 0; x < amount; ++x) {
    matchesList.push({
      me: User(),
      other: User(),
      matchRate: faker.random.number(100) || 1,
      distance: faker.random.number(50) || 1,
      type: matchType
    })
  }
  return matchesList
}
