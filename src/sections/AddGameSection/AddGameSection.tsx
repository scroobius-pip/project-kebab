import React, { Component } from 'react'
import { GameSelectCard, Container, TaskButton, DescriptionBox } from '../../components'
import { Row, Col, message } from 'antd'
import { ColProps } from 'antd/lib/col'

import searchGames from '../../functions/Graphql/searchGames';
import getUserGamesHas from 'functions/Graphql/getUserGamesHas'
import getUserGamesWant from 'functions/Graphql/getUserGamesWant';
import addGamesUpdateDescription from 'functions/Graphql/addGamesUpdateDescription';
import getUserDescription from 'functions/Graphql/getUserDescription';

const columnStyle: React.CSSProperties = {
  marginBottom: 40
}

const columnProps: ColProps = {
  xl: { span: 24 },
  sm: { span: 24 }
}

const GameSelectCardColumn = ({ children }: { children: JSX.Element }) => (
  <Col style={columnStyle} {...columnProps}>
    {children}
  </Col>
)

interface State {
  gameListDataGamesIHave: USER_GAME[]
  gameListDataGamesIWant: USER_GAME[]
  description: string
}

export default class extends Component<any, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      gameListDataGamesIHave: [],
      gameListDataGamesIWant: [],
      description: '',
    }
  }

  private descriptionChange = (description: string) => {
    this.setState({
      description
    })
  }

  private iHaveChange = (games: USER_GAME[]) => {
    this.setState({
      gameListDataGamesIHave: games
    })
  }

  private iWantChange = (games: USER_GAME[]) => {
    this.setState({
      gameListDataGamesIWant: games
    })
  }

  private saveButtonClicked = async () => {

    const games = [...this.state.gameListDataGamesIHave, ...this.state.gameListDataGamesIWant]
    const result = await addGamesUpdateDescription(games, this.state.description)
    if (result.error) {
      message.error(result.error)
    } else {
      message.success('Success 😆')
    }
  }

  public async componentDidMount() {
    this.setState({
      description: (await getUserDescription()),
      gameListDataGamesIHave: [],
      gameListDataGamesIWant: [],

    })
  }

  public render() {

    return (
      <Container>
        <Row gutter={40} justify="center" type="flex">
          <GameSelectCardColumn>
            <GameSelectCard getUserGame={getUserGamesHas} searchGames={searchGames} statusType='has' onChange={this.iHaveChange} heading="Games I Have" />
          </GameSelectCardColumn>
          <GameSelectCardColumn>
            <GameSelectCard getUserGame={getUserGamesWant} searchGames={searchGames} statusType='want' onChange={this.iWantChange} heading="Games I Want" />
          </GameSelectCardColumn>
          <Col style={columnStyle} span={24} xl={{ span: 24 }}>
            <DescriptionBox
              getDescription={getUserDescription}
              onChange={this.descriptionChange}
            />
          </Col>
          <Col style={{ textAlign: 'center' }} span={24}>
            <TaskButton
              size="large"
              type="primary"
              normalText="Save"
              loadingText="Saving"
              onClick={this.saveButtonClicked}
            />
          </Col>
        </Row>
      </Container>
    )
  }
}
