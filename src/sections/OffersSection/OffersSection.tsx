import React, { Component } from 'react'
import { Row, List, message, Col } from 'antd'
import { SortOffersSwitch, Container, OfferCard } from '../../components'
import mockOffers from './offersData.mock'

interface State {
  offers: OFFER_TYPE[]
  loading: boolean
  sortByStatus: OFFER_TYPE['status']
}

const acceptOffer = (offerId: string) => {
  message.info(`Accepted ${offerId}`)
}

const declineOffer = (offerId: string) => {
  message.info(`Declined ${offerId}`)
}

const completeOffer = (offerId: string) => {
  message.info(`Completed ${offerId}`)
}

const reportOffer = (offerId: string) => {
  message.warning(`Reported ${offerId}`)
}

const OfferCardItem = (props: OFFER_TYPE) => (
  // @ts-ignore
  <List.Item xl={4}>
    <OfferCard
      onReport={reportOffer}
      onAccept={acceptOffer}
      onDecline={declineOffer}
      onComplete={completeOffer}
      {...props}
    />
  </List.Item>
)

export default class extends Component<any, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      offers: [],
      loading: false,
      sortByStatus: 'pending'
    }
  }

  public async componentDidMount() {
    await this.getAndRenderOffers()
  }

  private async getAndRenderOffers() {
    this.setLoading(true)
    const matches = await this.getOffers(this.state.sortByStatus)
    this.setOffersState(matches)
    this.setLoading(false)
  }

  public componentDidUpdate(prevProps: any, prevState: State) {
    if (prevState.sortByStatus !== this.state.sortByStatus) {
      this.getAndRenderOffers()
    }
  }

  private async getOffers(status: OFFER_TYPE['status']): Promise<OFFER_TYPE[]> {
    const matches = await mockOffers(1, status)
    return matches
  }

  private setOffersState(offers: OFFER_TYPE[]) {
    this.setState({
      offers
    })
  }

  private setLoading(loading: boolean) {
    this.setState({
      loading
    })
  }

  private setSortBy = (sortByStatus: OFFER_TYPE['status']) => {
    this.setState({
      sortByStatus
    })
  }

  public render() {
    const { sortByStatus, loading, offers } = this.state
    return (
      <Container style={{ width: '100%', minHeight: '80vh' }}>
        <Row align="middle">
          <Col style={{ display: 'flex', justifyContent: 'center' }} span={24}>
            <SortOffersSwitch
              value={sortByStatus}
              sortChange={this.setSortBy}
              style={{ marginBottom: 40, }}
            />
          </Col>
          <Col span={24}>
            <List
              bordered={false}
              loading={loading}
              grid={{ xs: 1, sm: 1, md: 1, xl: 1, gutter: 48 }}
              dataSource={offers}
              renderItem={OfferCardItem}
            />
          </Col>
        </Row>
      </Container>
    )
  }
}
