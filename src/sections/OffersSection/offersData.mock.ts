import faker from 'faker'

const consoleTypes = ['PlayStation', 'PlayStation 3', 'Xbox', 'PlayStation 4']
const tradeType: tradeType[] = ['swap', 'sale']
const statusType: statusType[] = ['has', 'want']

const UserGame = (): USER_GAME => ({
  name: faker.lorem.word(),
  consoleType: faker.random.arrayElement(consoleTypes),
  id: faker.random.uuid(),
  tradeType: faker.random.arrayElement(tradeType),
  description: faker.lorem.sentence(),
  statusType: faker.random.arrayElement(statusType)
})

const UserInfo = (): USER_TYPE => ({
  description: faker.lorem.paragraph(),
  id: faker.random.uuid(),
  isPro: faker.random.boolean(),
  joinedDate: {
    month: faker.date.month(),
    year: faker.date
      .between('2005', '2018')
      .getFullYear()
      .toString()
  },
  location: {
    city: faker.address.city(),
    state: faker.address.stateAbbr()
  },
  noOfSuccessfulExchanges: faker.random.number(100),
  rating: {
    negative: 0,
    positive: faker.random.number(100)
  },
  userImageUrl: faker.internet.avatar(),
  userName: faker.internet.userName()
})

const User = () => ({
  info: UserInfo(),
  games: (() => {
    const games = []
    const gameCount = faker.random.number(5) || 1
    for (let x = 0; x < gameCount; ++x) {
      games.push(UserGame())
    }
    return games
  })()
})

export default (
  amount: number,
  status: OFFER_TYPE['status']
): Array<{
  me: USER_WITH_GAMES
  other: USER_WITH_GAMES
  offerId: string
  status: OFFER_TYPE['status']
}> => {
  const offersList = []
  for (let x = 0; x < amount; ++x) {
    offersList.push({
      me: User(),
      other: User(),
      offerId: faker.random.uuid(),
      status
    })
  }
  return offersList
}
