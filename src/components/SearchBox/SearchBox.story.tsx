import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import SearchBox from './'

import 'antd/dist/antd.css'
import searchGames from '../../functions/Graphql/searchGames';

action('searchItemClicked')



storiesOf('Search Box', module).add('Normal', () => (
  <SearchBox
    style={{ width: '100%' }}
    searchFunction={searchGames}
    onSelect={action('searchItemClicked')}
  />
))
