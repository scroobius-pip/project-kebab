//
import * as React from 'react'
import { Icon, Input, AutoComplete, Spin } from 'antd'
import generateColorFromString from '../../functions/generateColorFromString'
import { debounce } from 'lodash';
// import { SelectValue } from 'antd/lib/select'

interface Props {
  searchFunction: (searchText: string) => Promise<SEARCH_RESULT>
  onSelect: (value: GAME_TYPE) => any | void
  style?: React.CSSProperties
}

interface State {
  isLoading: boolean
  dataSource: JSX.Element[]
  autoCompleteValue: string
}

const loadingIcon = (
  <Spin indicator={<Icon type="loading" style={{ fontSize: 24 }} spin={true} />} />
)

const searchIcon = <Icon type="search" />

const Option = AutoComplete.Option
const OptionGroup = AutoComplete.OptGroup

class SearchBox extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      isLoading: false,
      dataSource: [],
      autoCompleteValue: ''
    }
    this.handleSearch = debounce(this.handleSearch, 500)
  }

  private renderResults = (searchResults: SEARCH_RESULT): JSX.Element[] => {
    const rendered = []
    for (const consoleType in searchResults) {
      if (consoleType.length > 1) {
        rendered.push(
          <OptionGroup
            key={consoleType}
            label={
              <p style={{ color: generateColorFromString(consoleType.toString()) }}>
                {consoleType}
              </p>
            }>
            {searchResults[consoleType].map(game => {
              return (
                <Option key={game.id + consoleType} value={JSON.stringify(game)}>
                  {game.name}
                </Option>
              )
            })}
          </OptionGroup>
        )
      }
    }

    return rendered
  }

  private handleSearch = async (searchText: string) => {
    this.setState({ isLoading: true })

    const searchResults = searchText ? await this.props.searchFunction(searchText) : {}
    this.setState({
      dataSource: this.renderResults(searchResults),
      isLoading: false
    })
  }

  private handleSelect = (gameString: string) => {
    this.props.onSelect(JSON.parse(gameString))
  }

  private onAutoCompleteChange = (autoCompleteValue: string) => {
    this.setState({ autoCompleteValue })
  }

  // private clearAutoComplete = () => {
  //   this.setState({ autoCompleteValue: '' })
  // }

  public render() {
    const { isLoading, dataSource } = this.state

    return (
      <AutoComplete
        onChange={this.onAutoCompleteChange}
        value={this.state.autoCompleteValue}
        className="certain-category-search"
        dropdownClassName="certain-category-search-dropdown"
        dropdownMatchSelectWidth={false}
        dropdownStyle={{ ...this.props.style }}
        size="large"
        style={this.props.style}
        // style={{ width: '100%' }}
        dataSource={dataSource}
        placeholder="Search Games"
        optionLabelProp="value"
        onSearch={this.handleSearch}
        onSelect={this.handleSelect}>
        {/* show a loading sign when searchfunction is being run */}
        <Input suffix={isLoading ? loadingIcon : searchIcon} />
      </AutoComplete>
    )
  }
}

export default SearchBox
