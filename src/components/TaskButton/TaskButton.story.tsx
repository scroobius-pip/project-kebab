import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import TaskButton from './index'
import 'antd/dist/antd.css'

action('addClicked')

async function stall(stallTime = 3000) {
  await new Promise(resolve => setTimeout(resolve, stallTime))
}

storiesOf('Task Button', module).add('Normal', () => (
  <TaskButton
    onClick={async () => {
      await stall(5000)
      console.log('nice')
    }}
    normalText="Save"
    loadingText="Saving"
    type="primary"
    size="large"
  />
))
