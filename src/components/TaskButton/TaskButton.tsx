import React from 'react'
import { Button } from 'antd'
import { ButtonProps } from 'antd/lib/button'

type Props = {
  normalText: string
  loadingText: string
  onClick: () => Promise<any> | any
  style?: React.CSSProperties
} & ButtonProps

interface State {
  loading: boolean
}

export default class extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      loading: false
    }
  }

  private runTask = async () => {
    this.setState({
      loading: true
    })

    await this.props.onClick()

    this.setState({
      loading: false
    })
  }

  public render() {
    const { onClick, normalText, loadingText, style, ...props } = this.props
    return (
      <Button style={style} onClick={this.runTask} {...props} loading={this.state.loading}>
        {this.state.loading ? loadingText : normalText}
      </Button>
    )
  }
}
