import React from 'react'
import { Carousel } from 'antd'



export default ({ children }: { children: JSX.Element[] }) => <Carousel slidesToShow={1} dots>{children}</Carousel>
