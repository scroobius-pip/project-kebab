import React from 'react';
import { Icon, Spin } from 'antd';


export default ({ children, loading }: { children: JSX.Element; loading: boolean }) => {
    const antIcon = <Icon type="loading" style={{ fontSize: 50 }} spin={true} />

    return (
        <Spin indicator={antIcon} spinning={loading}>
            {children}
        </Spin>
    )
}

