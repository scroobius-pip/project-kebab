import React from 'react'

interface Props {
  children: JSX.Element[] | JSX.Element
  style?: React.CSSProperties
}

export default ({ children, style }: Props) => (
  <div
    style={{
      borderRadius: 10,

      padding: 20,
      backgroundColor: '#F7F7F7',
      ...style
    }}>
    {children}
  </div>
)
