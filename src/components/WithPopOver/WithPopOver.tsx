//
import React, { Component } from 'react'
import { Popconfirm } from 'antd'

interface Props {
  children: JSX.Element
  title: JSX.Element | string
  onConfirm: () => any
  onCancel: () => any
}

interface State {
  visible: boolean
}

class WithPopOver extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      visible: false
    }

    this.handleVisibleChange = this.handleVisibleChange.bind(this)
  }

  private handleVisibleChange(visible: boolean) {
    this.setState({ visible })
  }

  public render() {
    const { children, title, onConfirm, onCancel } = this.props

    return (
      <Popconfirm
        // icon={<Icon type=''/>}
        onCancel={onCancel}
        onConfirm={onConfirm}
        title={title}
        trigger="click"
        visible={this.state.visible}
        onVisibleChange={this.handleVisibleChange}>
        {children}
      </Popconfirm>
    )
  }
}

export default WithPopOver
