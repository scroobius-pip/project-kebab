import React from 'react'
import { storiesOf } from '@storybook/react'

import GameList from './'
import GameCard from '../GameCard'
import GameCheckBox from '../GameCheckBox'

import 'antd/dist/antd.css'

const gameListData: USER_GAME[] = [
  {
    name: 'Red Dead Redemption 2',
    consoleType: 'PlayStation 4',
    imageUrl:
      'https://rockstarintel.com/wp-content/uploads/2018/08/de6fee07809ff0c1e2017552090dcf72460e46e7.jpg',
    id: '1',
    tradeType: 'swap',
    description:
      'Lorem https://imgur.com/a/dwRQgqu ipsum dolor sit amet consectetur adipisicing elit. Nobis facilis voluptatem est expedita libero. Culpa cumque sint quas veritatis voluptatibus!'
  },
  {
    name: 'Red Dead Redemption 2',
    consoleType: 'PlayStation 3',
    imageUrl:
      'https://rockstarintel.com/wp-content/uploads/2018/08/de6fee07809ff0c1e2017552090dcf72460e46e7.jpg',
    id: '2',
    tradeType: 'swap',
    description:
      'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, suscipit dolorum iusto tempore nam aliquid similique optio non omnis quibusdam ad ut fuga sit laborum eum quisquam! Cumque, tempore laborum?'
  },
  {
    name: 'Red Dead Redemption 2',
    consoleType: 'PlayStation 2',
    imageUrl:
      'https://rockstarintel.com/wp-content/uploads/2018/08/de6fee07809ff0c1e2017552090dcf72460e46e7.jpg',
    id: '3',
    tradeType: 'sale',
    description: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptas, aut.'
  },
  {
    name: 'Red Dead Redemption 2',
    consoleType: 'XBOX 360',
    imageUrl:
      'https://rockstarintel.com/wp-content/uploads/2018/08/de6fee07809ff0c1e2017552090dcf72460e46e7.jpg',
    id: '4',
    tradeType: 'sale',
    description: 'Lorem ipsum dolor sit amet.'
  },
  {
    name: 'The Adventures of Batman & Robin',
    consoleType: 'SNES',
    imageUrl: '//images.igdb.com/igdb/image/upload/t_thumb/ms4hlrdthun4zfsbty1x.jpg',
    id: '5',
    tradeType: 'sale',
    description: ''
  },
  {
    name: 'Red Dead Redemption 2',
    consoleType: 'PlayStation 4',
    imageUrl:
      'https://rockstarintel.com/wp-content/uploads/2018/08/de6fee07809ff0c1e2017552090dcf72460e46e7.jpg',
    id: '6',
    tradeType: 'swap',
    description: ''
  }
]

storiesOf('Game List', module)
  .add('Game Card', () => (
    <GameList
      // itemClicked={action('removeClicked')}
      data={gameListData}
      ListItem={GameCard}
      listItemProps={{
        removeClicked: (id: any) => {
          console.log(id)
        }
      }}
    />
  ))
  .add('Game Checkbox', () => (
    <GameList
      data={gameListData}
      ListItem={GameCheckBox}
      listItemProps={{
        onChange: (id: any) => {
          // action('checkboxClicked')
          console.log(id)
        }
      }}
      header={
        <div>
          <span>Freddy95</span>
          <h1>I want:</h1>
        </div>
      }
    />
  ))
