//
import React from 'react'
import { List } from 'antd'
import Container from '../Container'

interface Props {
  data: any[]
  header?: any
  ListItem: any
  // listItemProps: {}
  style?: React.CSSProperties
}

const GameList = <ItemProps extends {}>(props: Props & { listItemProps: ItemProps }) => {
  const { data, header, ListItem, listItemProps, style } = props
  const renderItem = (item: Props['data']) => {
    return <ListItem {...listItemProps} {...item} />
  }

  return (
    <Container style={{ ...style, overflowY: 'auto' }}>
      {header}

      <List locale={{ emptyText: 'Add Games' }} dataSource={data} renderItem={renderItem} />
    </Container>
  )
}

export default GameList
