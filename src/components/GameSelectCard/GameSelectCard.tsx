import React from 'react'
import { Row, Col, Card } from 'antd'
import SearchBox from '../SearchBox'
import GameList from '../GameList'
import GameCard from '../GameCard'
import { unionBy, differenceWith, isEqual } from 'lodash'
import { GameCardProps } from 'components/GameCard/GameCard'
import WithSpin from '../WithSpin';

interface Props {
  heading: string
  onChange: (games: USER_GAME[]) => any
  statusType: statusType
  searchGames: (searchString: string) => Promise<SEARCH_RESULT>
  getUserGame: () => Promise<USER_GAME[]>

}

interface GameListWithCardProps {
  gameListData: USER_GAME[]
  removeClicked: (edit: string) => any
  editDescription: (id: string, details: FORM_FIELD_GAME_DETAILS) => any
}

interface State {
  loading: boolean
  gameListData: USER_GAME[]
  INITIALDATA: USER_GAME[]

}


const GameListWithCard = ({
  gameListData,
  removeClicked,
  editDescription
}: GameListWithCardProps) => {
  return (
    <GameList<{
      removeClicked: GameCardProps['removeClicked']
      editDescription: GameCardProps['editDescription']
    }>
      style={{ height: 600, maxHeight: 600 }}
      data={gameListData}
      ListItem={GameCard}
      listItemProps={{
        removeClicked,
        editDescription
      }}
    />
  )
}

export default class extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      loading: true,
      gameListData: [],
      INITIALDATA: []

    }
  }

  private addGame = (game: USER_GAME) => {
    this.setState({
      gameListData: unionBy(this.state.gameListData, [game], 'id')
    })
  }

  public async componentDidMount() {
    this.setState({
      loading: true
    })
    const initialGames = await this.props.getUserGame()
    this.setState({
      loading: false,
      gameListData: initialGames,
      INITIALDATA: initialGames
    })
  }


  public componentDidUpdate(prevProps: Props, prevState: State) {
    const changedGames = differenceWith(this.state.gameListData, this.state.INITIALDATA, isEqual)
    this.props.onChange(changedGames)

  }

  private deleteGame = (id: string) => {
    this.setState({
      gameListData: [...this.state.gameListData.filter(game => game.id !== id)]
    })
  }

  private searchSelect = (game: GAME_TYPE) => {
    const userGameTypeFromGameType: USER_GAME = { ...game, description: '', tradeType: 'swap', statusType: this.props.statusType }

    this.addGame(userGameTypeFromGameType)
  }

  private editGameDescription = (id: string, details: FORM_FIELD_GAME_DETAILS) => {
    const newGameList: USER_GAME[] = this.state.gameListData.map(game => {
      if (game.id === id) {
        return { ...game, ...details }
      }
      return { ...game }
    })

    this.setState({
      gameListData: newGameList
    })
  }

  public render() {
    return (
      <Card
        style={{
          borderRadius: 5
        }}>
        <WithSpin loading={this.state.loading}>
          <div
            style={{
              padding: 20
            }}>
            <h1>{this.props.heading}</h1>
            <Row
              style={{ marginTop: 20 }}
              gutter={16}
              type="flex"
              justify="space-between"
              align="middle">
              <Col span={24}>
                <SearchBox
                  style={{ width: '100%', minWidth: 300, maxWidth: 600 }}
                  onSelect={this.searchSelect}
                  searchFunction={this.props.searchGames}
                />
              </Col>
            </Row>
            <div style={{ marginTop: 20, height: '100%' }}>

              <GameListWithCard
                editDescription={this.editGameDescription}
                removeClicked={this.deleteGame}
                gameListData={this.state.gameListData}
              />

            </div>
          </div>
        </WithSpin>
      </Card>
    )
  }
}
