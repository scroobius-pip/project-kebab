import React from 'react'
import { storiesOf } from '@storybook/react'

import GameSelectCard from './'
import 'antd/dist/antd.css'
import searchGames from '../../functions/Graphql/searchGames';
import getUserGamesHas from 'functions/Graphql/getUserGamesHas';


// const gameListData: USER_GAME[] = [
//   {
//     name: 'Red Dead Redemption 2',
//     consoleType: 'PlayStation 4',
//     imageUrl: 'https://images.igdb.com/igdb/image/upload/t_thumb/qrprlkkmeiqlwfjmvnwb.jpg',
//     id: '1',
//     tradeType: 'swap',
//     description: ''
//   }
// ]

storiesOf('Game Select Card', module).add('Normal', () => (
  <GameSelectCard
    getUserGame={getUserGamesHas}
    onChange={games => {
      console.log(games)
    }}
    searchGames={searchGames}
    statusType='has'
    heading="Games I Have"
  />
))
