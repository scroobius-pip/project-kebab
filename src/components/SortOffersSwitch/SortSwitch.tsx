import React from 'react'
import { Radio } from 'antd'
import { RadioChangeEvent } from 'antd/lib/radio'

interface Props {
  style?: React.CSSProperties
  sortChange: (sortType: OFFER_TYPE['status']) => void
  value: OFFER_TYPE['status']
}

export default class extends React.Component<Props, any> {
  constructor(props: Props) {
    super(props)
  }

  private handleSortChange = (e: RadioChangeEvent) => {
    const sortBy = e.target.value
    this.props.sortChange(sortBy)
  }

  render() {
    return (
      <Radio.Group
        style={this.props.style}
        buttonStyle="solid"
        size="large"
        onChange={this.handleSortChange}
        value={this.props.value}>
        <Radio.Button value="pending">Pending</Radio.Button>
        <Radio.Button value="ongoing">Ongoing</Radio.Button>
        <Radio.Button value="completed">Completed</Radio.Button>
        <Radio.Button value="cancelled">Cancelled</Radio.Button>
      </Radio.Group>
    )
  }
}
