import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import SortSwitch from './'

import 'antd/dist/antd.css'

action('searchItemClicked')

storiesOf('Sort Matches Switch', module).add('Normal', () => (
  <SortSwitch
    value="matchRate"
    sortChange={sortBy => {
      console.log(sortBy)
    }}
  />
))
