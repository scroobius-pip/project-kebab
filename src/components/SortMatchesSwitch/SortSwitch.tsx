import React from 'react'
import { Radio, Icon } from 'antd'
import { RadioChangeEvent } from 'antd/lib/radio'

interface Props {
  style?: React.CSSProperties
  sortChange: (sortType: MATCH_TYPE['type']) => void
  value: MATCH_TYPE['type']
}

export default class extends React.Component<Props, any> {
  constructor(props: Props) {
    super(props)
  }

  private handleSortChange = (e: RadioChangeEvent) => {
    const sortBy = e.target.value
    this.props.sortChange(sortBy)
  }

  render() {
    return (
      <Radio.Group
        style={this.props.style}
        buttonStyle="solid"
        size="large"
        onChange={this.handleSortChange}
        value={this.props.value}>
        <Radio.Button value="matchRate">
          <Icon style={{ fontSize: 18 }} type="block" /> Match Rate
        </Radio.Button>

        <Radio.Button value="distance">
          <Icon theme="filled" style={{ fontSize: 18 }} type="car" /> Distance
        </Radio.Button>
      </Radio.Group>
    )
  }
}
