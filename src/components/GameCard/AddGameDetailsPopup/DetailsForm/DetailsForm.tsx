import React, { Component } from 'react'
import { Form, Input, Radio } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import FormItem from 'antd/lib/form/FormItem'

interface DetailsFormProps extends FormComponentProps {
  description: string
  tradeType: tradeType
  onChange: (fields: FORM_FIELD_GAME_DETAILS) => any
}

class DetailsForm extends Component<DetailsFormProps, any> {
  public render() {
    const {
      form: { getFieldDecorator },
      description,
      tradeType
    } = this.props

    return (
      <Form layout="vertical">
        <FormItem help="Add description that helps describe the game." label="Description">
          {getFieldDecorator('description', { initialValue: description })(<Input.TextArea />)}
        </FormItem>
        <FormItem label="Trade Type">
          {getFieldDecorator('tradeType', {
            initialValue: tradeType
          })(
            <Radio.Group>
              <Radio value="swap">Swap</Radio>
              <Radio value="sale">Sale</Radio>
            </Radio.Group>
          )}
        </FormItem>
      </Form>
    )
  }
}

export default Form.create({
  onFieldsChange(
    props: DetailsFormProps,
    changedFields: { description: { value: string } } | { tradeType: { value: string } }
  ) {
    const key: string = Object.keys(changedFields)[0]
    props.onChange({
      [key]: changedFields[key].value
    })
  }
})(DetailsForm)
