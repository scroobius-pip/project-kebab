import React from 'react'
import { storiesOf } from '@storybook/react'

import DetailsForm from './'
import 'antd/dist/antd.css'

storiesOf('Details Form', module).add('Normal', () => (
  <DetailsForm
    onChange={(all: any) => {
      console.log(all)
    }}
    description="My game is nice and pretty i will only swap it for a fortune"
    tradeType="sale"
  />
))
