import React from 'react'
import { storiesOf } from '@storybook/react'
import { Button } from 'antd'
import AddGameDetailsPopup from './'
import 'antd/dist/antd.css'

storiesOf('Add Game Details Popover', module).add('Normal', () => (
  <AddGameDetailsPopup
    onClose={fields => {
      console.log(fields)
    }}
    description=""
    tradeType="sale">
    <Button>Details</Button>
  </AddGameDetailsPopup>
))
