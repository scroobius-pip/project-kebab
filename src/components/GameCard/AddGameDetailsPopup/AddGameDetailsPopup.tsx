import * as React from 'react'
import DetailsForm from './DetailsForm/index'
import { Popover } from 'antd'

interface Props {
  children: JSX.Element
  tradeType: tradeType
  description: string
  onClose: (fields: FORM_FIELD_GAME_DETAILS) => any
}

interface State {
  description: string
  tradeType: tradeType
}

export default class extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      description: this.props.description,
      tradeType: this.props.tradeType
    }
  }

  private onChange = (field: FORM_FIELD_GAME_DETAILS) => {
    this.setState({ ...this.state, ...field })
  }

  private onVisibleChange = (visible: boolean) => {
    if (!visible) {
      this.props.onClose(this.state)
    }
  }

  public render() {
    const { children } = this.props
    const { description, tradeType } = this.state
    return (
      <Popover
        onVisibleChange={this.onVisibleChange}
        trigger="click"
        content={
          <DetailsForm tradeType={tradeType} description={description} onChange={this.onChange} />
        }>
        {children}
      </Popover>
    )
  }
}
