import React from 'react'
import { storiesOf } from '@storybook/react'

import GameCard from './'
import 'antd/dist/antd.css'

storiesOf('Game Card', module).add('Normal', () => (
  <GameCard
    description="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Totam illum accusantium sapiente nobis asperiores reprehenderit."
    tradeType="sale"
    consoleType="PS4"
    id="1"
    imageUrl="https://rockstarintel.com/wp-content/uploads/2018/08/de6fee07809ff0c1e2017552090dcf72460e46e7.jpg"
    removeClicked={id => {
      console.log(id)
    }}
    editDescription={(id, details) => {
      console.log(id, details)
    }}
    name="Uncharted 4: A Thief's End"
  />
))
