//
import React from 'react'
import { Button, Card, Icon, Row, Col, Tooltip, Spin } from 'antd'
import AddGameDetailsPopup from './AddGameDetailsPopup'
import Img from 'react-image'
import Tags from '../Tags'

const { TradeTag, ConsoleTag } = Tags

export type GameCardProps = {
  editDescription: (id: string, details: FORM_FIELD_GAME_DETAILS) => any
  removeClicked: (id: string) => any
} & USER_GAME

const LoadingIcon = (
  <Spin indicator={<Icon type="loading" style={{ fontSize: 24 }} spin={true} />} />
)

const GameImage = ({ imageUrl }: { imageUrl: string }) => (
  <Img loader={LoadingIcon} style={{ width: 60, height: 60 }} src={imageUrl} />
)

const EditButton = ({
  tradeType,
  description,
  id,
  onEdit
}: {
  tradeType: tradeType
  description: string
  id: string
  onEdit: (id: string, details: FORM_FIELD_GAME_DETAILS) => any
}) => {
  const editDescription = (details: FORM_FIELD_GAME_DETAILS) => {
    onEdit(id, details)
  }

  return (
    <Tooltip title="Add a description to your game">
      <AddGameDetailsPopup
        onClose={editDescription}
        tradeType={tradeType}
        description={description}>
        <Button type="ghost" shape="circle-outline">
          <Icon theme="outlined" type="edit" />
        </Button>
      </AddGameDetailsPopup>
    </Tooltip>
  )
}

const RemoveButton = ({ onRemove, id }: { onRemove: (id: string) => any; id: string }) => {
  const remove = () => {
    onRemove(id)
  }

  return (
    <Button onClick={remove} type="danger" shape="circle">
      <Icon theme="outlined" type="delete" />
    </Button>
  )
}

export default (props: GameCardProps) => {
  const {
    consoleType,
    imageUrl = 'https://images.igdb.com/igdb/image/upload/t_thumb/nocover_qhhlj6.jpg',
    name,
    editDescription,
    removeClicked,
    id,
    tradeType,
    description
  } = props

  return (
    <Card hoverable={true} style={{ marginBottom: 15, borderRadius: 5 }}>
      <Row type="flex" justify="space-between" align="middle">
        <Col span={4}>
          <GameImage imageUrl={imageUrl} />
        </Col>
        <Col span={16}>
          <Row>
            <Col span={24}>
              <h3>{name}</h3>
            </Col>
            <Col span={24}>
              <ConsoleTag consoleType={consoleType} />
              <TradeTag tradeType={tradeType} />
            </Col>
          </Row>
        </Col>
        <Col span={4}>
          <Col span={12}>
            <EditButton
              onEdit={editDescription}
              tradeType={tradeType}
              description={description}
              id={id}
            />
          </Col>
          <Col span={12}>
            <RemoveButton onRemove={removeClicked} id={id} />
          </Col>
        </Col>
      </Row>
    </Card>
  )
}
