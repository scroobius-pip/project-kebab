//
import React from 'react'
import { Modal } from 'antd'

interface Props {
  children: JSX.Element | JSX.Element[]
  style: React.CSSProperties
}

export default ({ children, style }: Props) => {
  return (
    <Modal
      style={style}
      bodyStyle={{ padding: 0, height: style.height }}
      closable={false}
      destroyOnClose={true}
      // centered={true}

      width={style.width}
      footer={null}
      maskClosable={false}
      visible={true}>
      {children}
    </Modal>
  )
}
