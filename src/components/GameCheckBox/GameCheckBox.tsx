//
import React from 'react'
import { Checkbox, Row, Col, Popover } from 'antd'
import { CheckboxChangeEvent } from 'antd/lib/checkbox'
import Linkify from 'react-linkify'
import Tags from '../Tags'

const TradeTag = Tags.TradeTag
const ConsoleTag = Tags.ConsoleTag

export type GameCheckboxProps = USER_GAME & {
  onChange: (id: string) => any
  selected?: boolean
  id: string
  disabled?: boolean
  checkBoxVisible?: boolean
  tradeType: tradeType
}

const GameCheckBox = (props: GameCheckboxProps) => {
  const {
    consoleType,
    name,
    onChange,
    selected = false,
    id,
    disabled,
    checkBoxVisible = true,
    tradeType,
    description
  } = props

  function _onChange(e: CheckboxChangeEvent) {
    onChange(e.target.value)
  }

  const Details = () => (
    <Linkify properties={{ target: '_blank' }}>
      <p>{description}</p>
    </Linkify>
  )

  return (
    <Row
      style={{
        padding: 15,
        marginBottom: 10,
        marginLeft: -10,
        marginRight: -10,
        backgroundColor: '#F3F1F1'
      }}
      type="flex"
      justify="space-between">
      <Popover
        mouseEnterDelay={0.1}
        overlayStyle={{ maxWidth: 400 }}
        placement="top"
        title={`Details - ${name}`}
        content={<Details />}>
        <Col span={22}>
          <p style={{ fontWeight: 600, margin: 0, marginBottom: 5 }}>{`${name}`}</p>
          <ConsoleTag consoleType={consoleType} />
          <TradeTag tradeType={tradeType} />
        </Col>
      </Popover>
      {checkBoxVisible ? (
        <Col>
          <Checkbox value={id} disabled={disabled} defaultChecked={selected} onChange={_onChange} />
        </Col>
      ) : null}
    </Row>
  )
}

export default GameCheckBox
