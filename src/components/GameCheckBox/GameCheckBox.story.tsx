import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

import GameCheckBox from './'
import 'antd/dist/antd.css'

storiesOf('Game Check Box', module).add('Normal', () => (
  <GameCheckBox
    description="Lorem ipsum, dolor sit amet consectetur adipisicing elit. Saepe nam, nostrum hic id vero esse rem molestias facilis fugiat aut"
    id="3"
    consoleType="PS4"
    name="Rise of the Tomb Raider"
    onChange={(id: string) => action('checkboxClicked')}
    tradeType="swap"
  />
))
