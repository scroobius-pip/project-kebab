import React from 'react'
import { storiesOf } from '@storybook/react'
import Tags from './'

import 'antd/dist/antd.css'

const { ConsoleTag, TradeTag } = Tags

storiesOf('Tags', module)
  .add('ConsoleTag', () => <ConsoleTag consoleType="Playstation 4" />)
  .add('TradeTag', () => <TradeTag tradeType="sale" />)
