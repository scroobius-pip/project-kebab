import React from 'react'
import { generateColorFromString } from '../../functions/index'
import { Tag } from 'antd'

const ConsoleTag = ({ consoleType }: { consoleType: string }) => (
  <Tag color={generateColorFromString(consoleType.toString())}>{consoleType}</Tag>
)

const TradeTag = ({ tradeType }: { tradeType: tradeType }) => (
  <Tag>{tradeType === 'sale' ? 'Sale' : 'Swap'}</Tag>
)

export default { ConsoleTag, TradeTag }
