import React from 'react'
import { Button, Icon } from 'antd'
import WithModal from '../WithModal'

// const SocialButton = ({ platform }: { platform: string }) => {
//   const platforms = {
//     facebook: {
//       icon: 'facebook',
//       color: '',
//       text: 'Sign in with Facebook'
//     },
//     google: {
//       icon: 'google',
//       color: '',
//       text: 'Sign in with Google'
//     }
//   }

//   const { icon, color, text } = platforms[platform]

//   return (
//     <Button style={{ marginBottom: 10 }} block size="large">
//       <Icon style={{ color }} type={icon} />
//       {text}
//       <Icon type="arrow-right" />
//     </Button>
//   )
// }

const LoginCard = () => (
  <div style={{ display: 'flex', height: '100%' }}>
    <div style={{ width: '40%', padding: 50, textAlign: 'center' }}>
      <h1 style={{ fontWeight: 'bold' }}>Sign In </h1>
      <div style={{ marginTop: 40 }}>
        <Button shape="circle" style={{ height: 50, width: 50 }}>
          <Icon style={{ fontSize: 30 }} type="reddit" />
        </Button>

        <Button shape="circle" style={{ height: 50, width: 50, marginLeft: 20 }}>
          <Icon style={{ fontSize: 30 }} type="twitter" />
        </Button>
      </div>
    </div>
    <div
      style={{
        width: '60%',
        backgroundColor: '#5B56E0',
        color: 'white',
        padding: 50,
        textAlign: 'right'
      }}>
      <div>
        <img
          style={{ width: 40, marginRight: 20, verticalAlign: 'middle' }}
          src={require('../../assets/logo1.svg')}
        />
        <span style={{ fontSize: 20, fontWeight: 600 }}>Project Kebab</span>
      </div>
      <h1
        style={{
          color: 'white',
          maxWidth: 500,
          margin: 'auto',
          marginTop: 100,
          justifyContent: 'left'
        }}>
        Sell or Swap Video Games With Gamers Around You
      </h1>
    </div>
  </div>
)

export default () => {
  return (
    <WithModal style={{ width: '50vw', height: '50vh' }}>
      <LoginCard />
    </WithModal>
  )
}
