import React from 'react'
import { Row, Col, Button, Icon } from 'antd'
import GameList from '../GameList'
import GameCheckBox from '../GameCheckBox'
import WithPopOver from '../WithPopOver'

export interface HiddenSectionProps {
  loading: boolean
  submitDisabled?: () => boolean
  userName: string
  gameListOther: GAME_TYPE[]
  gameListMe: GAME_TYPE[]
  onAccept: () => any
  onComplete: () => any
  onDecline: () => any
  onReport: () => any
  status: OFFER_TYPE['status']
}

export default ({
  loading,
  onAccept,
  onComplete,
  onDecline,
  onReport,
  userName,
  gameListOther,
  gameListMe,
  status
}: HiddenSectionProps) => {
  const onCancel = () => {}

  return (
    <div>
      <Row style={{ marginTop: 20 }} gutter={16}>
        <Col span={12}>
          <GameList
            style={{ maxHeight: 400 }}
            data={gameListMe}
            ListItem={GameCheckBox}
            listItemProps={{ checkBoxVisible: false }}
            header={
              <div>
                <span>Me</span>
                <h1>You give:</h1>
              </div>
            }
          />
        </Col>
        <Col span={12}>
          <GameList
            style={{ maxHeight: 400 }}
            data={gameListOther}
            ListItem={GameCheckBox}
            listItemProps={{ checkBoxVisible: false }}
            header={
              <div>
                <span>{userName}</span>
                <h1>You get:</h1>
              </div>
            }
          />
        </Col>
      </Row>

      <Row style={{ marginTop: 20 }} type="flex" align="middle" justify="center">
        {status === 'pending' ? (
          <Button.Group size="default">
            <WithPopOver title="Are You Sure ?" onCancel={onCancel} onConfirm={onDecline}>
              <Button type="danger">
                <Icon type="close" theme="twoTone" />
                Decline Offer
              </Button>
            </WithPopOver>
            <WithPopOver
              title={
                <div>
                  <h4>Are You Sure?</h4>
                  <p>Make sure to read safety procedures for conducting game swaps:</p>
                  <a href="https://www.reddit.com/r/gameswap/comments/5l4r8b/mod_safe_swapping_thread_how_to_identify_and/">
                    Safety Procedures
                  </a>
                </div>
              }
              onCancel={onCancel}
              onConfirm={onAccept}>
              <Button loading={loading} type="primary">
                Accept Offer
                <Icon type="check" theme="twoTone" />
              </Button>
            </WithPopOver>
          </Button.Group>
        ) : status === 'ongoing' ? (
          <Button.Group size="default">
            <WithPopOver title="Are You Sure ?" onCancel={onCancel} onConfirm={onReport}>
              <Button type="danger">
                <Icon type="stop" />
                Report User
              </Button>
            </WithPopOver>
            <WithPopOver
              title={
                <div>
                  <h4>Are You Sure?</h4>
                  <p>Make sure to read safety procedures for conducting game swaps:</p>
                  <a href="https://www.reddit.com/r/gameswap/comments/5l4r8b/mod_safe_swapping_thread_how_to_identify_and/">
                    Safety Procedures
                  </a>
                </div>
              }
              onCancel={onCancel}
              onConfirm={onComplete}>
              <Button loading={loading} type="primary">
                Complete Offer
                <Icon type="check" theme="twoTone" />
              </Button>
            </WithPopOver>
          </Button.Group>
        ) : null}
      </Row>
    </div>
  )
}
