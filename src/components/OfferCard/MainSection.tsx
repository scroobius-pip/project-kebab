import React, { Fragment } from 'react'
import { Row, Col, Icon, Tag } from 'antd'
import ProfileImage from '../ProfileImage'

export interface MainSectionProps extends USER_TYPE {
  noMyGames: number
  noOtherGames: number
  status: OFFER_TYPE['status']
}

export default ({
  location: { city, state },
  userName,
  userImageUrl,
  status,
  noMyGames,
  noOtherGames,
  noOfSuccessfulExchanges,
  isPro,
  joinedDate,
  rating,
  id
}: MainSectionProps) => {
  // span of outer <Col> should total 22

  const statusDisplay = {
    ongoing: {
      color: 'orange',
      text: 'Ongoing'
    },
    pending: {
      color: 'grey',
      text: 'Pending'
    },
    completed: {
      color: 'green',
      text: 'Completed'
    },
    cancelled: {
      color: 'red',
      text: 'Cancelled'
    }
  }

  const statusTag = (
    <Tag
      style={{ position: 'absolute', zIndex: 100, top: -10, right: 40 }}
      color={statusDisplay[status].color}>
      {statusDisplay[status].text}
    </Tag>
  )

  return (
    <Fragment>
      {statusTag}
      <Col style={{ boxSizing: 'border-box' }} span={8}>
        <Row type="flex" gutter={16} style={{ boxSizing: 'border-box' }}>
          <Col span={12} style={{ textAlign: 'center' }}>
            <div
              style={{
                backgroundColor: '#F7F7F7',
                borderRadius: 5,
                padding: 5,
                boxSizing: 'border-box'
              }}>
              <h2>{noMyGames}</h2>
              <span>You Give</span>
            </div>
          </Col>
          <Col span={12} style={{ textAlign: 'center' }}>
            <div
              style={{
                backgroundColor: '#F7F7F7',
                borderRadius: 5,
                padding: 5,
                boxSizing: 'border-box'
              }}>
              <h2>{noOtherGames}</h2>
              <span>You Get</span>
            </div>
          </Col>
        </Row>
      </Col>
      <Col span={14} offset={0}>
        <Row>
          <Col style={{ userSelect: 'none', textAlign: 'right' }} span={14}>
            <Col>
              <Icon />
              <span>{`${city},${state}`}</span>
            </Col>
            <Col>
              <h2>{userName}</h2>
            </Col>
          </Col>

          <Col span={5} offset={2}>
            <ProfileImage
              id={id}
              rating={rating}
              isPro={isPro}
              userImageUrl={userImageUrl}
              joinedDate={joinedDate}
              noOfSuccessfulExchanges={noOfSuccessfulExchanges}
            />
          </Col>
        </Row>
      </Col>
    </Fragment>
  )
}
