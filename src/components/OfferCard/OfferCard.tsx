import React, { Component } from 'react'
import RevealCard from '../RevealCard'
import HiddenSection, { HiddenSectionProps } from './HiddenSection'
import MainSection, { MainSectionProps } from './MainSection'

interface State {
  isLoading: boolean
  // gamesMe: GAME_TYPE[]
  // gamesOther: GAME_TYPE[]
}

type Props = {
  onAccept: (offerId: string) => any
  onDecline: (offerId: string) => any
  onComplete: (offerId: string) => any
  onReport: (offerId: string) => any
} & OFFER_TYPE

class OfferCard extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      // gamesOther: this.props.me.games,
      // gamesMe: this.props.other.games,
      isLoading: false
    }
  }

  private acceptOffer = () => {
    this.props.onAccept(this.props.offerId)
  }

  private completeOffer = () => {
    this.props.onComplete(this.props.offerId)
  }

  private declineOffer = () => {
    this.props.onDecline(this.props.offerId)
  }

  private reportOffer = () => {
    this.props.onReport(this.props.offerId)
  }

  public render() {
    const {
      status,
      me: { games: gameListMe },
      other: {
        info: {
          id,
          userImageUrl,
          userName,
          isPro,
          joinedDate,
          noOfSuccessfulExchanges,
          rating,
          location,
          description
        },
        games: gameListOther
      }
    } = this.props

    return (
      <RevealCard<MainSectionProps, HiddenSectionProps>
        style={{}}
        MainSection={MainSection}
        HiddenSection={HiddenSection}
        MainSectionProps={{
          id,
          location,
          userName,
          userImageUrl,
          status,
          noMyGames: this.props.me.games.length,
          noOtherGames: this.props.other.games.length,
          isPro,
          joinedDate,
          noOfSuccessfulExchanges,
          rating,
          description
        }}
        HiddenSectionProps={{
          userName,
          loading: this.state.isLoading,
          onAccept: this.acceptOffer,
          onDecline: this.declineOffer,
          onComplete: this.completeOffer,
          onReport: this.reportOffer,
          gameListMe,
          gameListOther,
          status
        }}
      />
    )
  }
}

export default OfferCard
