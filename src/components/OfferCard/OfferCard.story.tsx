import React from 'react'
import { storiesOf } from '@storybook/react'

import OfferCard from './'
import 'antd/dist/antd.css'

const gameListMe: USER_GAME[] = [
  {
    name: 'Red Dead Redemption 2',
    consoleType: 'PlayStation 4',
    id: '1',
    tradeType: 'swap',
    description: ''
  },
  {
    name: 'Spider Man',
    consoleType: 'PlayStation 4',
    id: '2',
    tradeType: 'sale',
    description: ''
  },
  {
    name: 'Battlefield 2',
    consoleType: 'PlayStation 3',
    id: '3',
    tradeType: 'swap',
    description: ''
  },
  {
    name: "Uncharted 3: Drake's Deception",
    consoleType: 'PlayStation 3',
    id: '4',
    tradeType: 'swap',
    description: ''
  },
  {
    name: 'Fifa 16',
    consoleType: 'PlayStation 3',
    id: '5',
    tradeType: 'swap',
    description: ''
  }
]

const meInfo: USER_TYPE = {
  rating: {
    positive: 10,
    negative: 0
  },
  id: '1',
  description: 'About me',
  isPro: true,
  joinedDate: { month: 'December', year: '2014' },
  noOfSuccessfulExchanges: 30,
  userImageUrl: 'https://avatars3.githubusercontent.com/u/11554364?s=460&v=4',
  location: { state: 'Fct', city: 'Abuja' },
  userName: 'Sim04ful'
}

const me = {
  info: meInfo,
  games: gameListMe
}
const gameListOther: USER_GAME[] = [
  {
    name: 'Rise of the Tomb Raider',
    consoleType: 'PlayStation 4',
    id: '7',
    tradeType: 'sale',
    description: ''
  },
  {
    name: 'Driveclub',
    consoleType: 'PlayStation 4',
    id: '8',
    tradeType: 'swap',
    description: ''
  },
  {
    name: 'Monster Hunter: World',
    consoleType: 'PlayStation 4',
    id: '9',
    tradeType: 'swap',
    description: ''
  },
  {
    name: "Assassin's Creed Odyssey",
    consoleType: 'PlayStation 4',
    id: '10',
    tradeType: 'swap',
    description: ''
  }
]

const otherInfo: USER_TYPE = {
  rating: {
    positive: 10,
    negative: 22
  },
  id: '2',
  description: 'About other person',
  isPro: false,
  joinedDate: { month: 'July', year: '2017' },
  noOfSuccessfulExchanges: 3,
  userImageUrl: 'https://avatars3.githubusercontent.com/u/11554364?s=460&v=4',
  location: { state: 'Fct', city: 'Abuja' },
  userName: 'Sim04ful'
}

const other = {
  info: otherInfo,
  games: gameListOther
}

storiesOf('Offer Card', module)
  .add('Completed', () => (
    <OfferCard
      me={me}
      other={other}
      onAccept={offerId => {
        console.log('Accepted ' + offerId)
      }}
      onDecline={offerId => {
        console.log('Declined ' + offerId)
      }}
      onComplete={offerId => {
        console.log('Completed ' + offerId)
      }}
      onReport={offerId => {
        console.log('Reported' + offerId)
      }}
      offerId="33kj3jk23d"
      status="completed"
    />
  ))
  .add('Ongoing', () => (
    <OfferCard
      me={me}
      other={other}
      onAccept={offerId => {
        console.log('Accepted ' + offerId)
      }}
      onDecline={offerId => {
        console.log('Declined ' + offerId)
      }}
      onComplete={offerId => {
        console.log('Completed ' + offerId)
      }}
      onReport={offerId => {
        console.log('Reported' + offerId)
      }}
      offerId="33kj3jk23d"
      status="ongoing"
    />
  ))
  .add('Pending', () => (
    <OfferCard
      me={me}
      other={other}
      onAccept={offerId => {
        console.log('Accepted ' + offerId)
      }}
      onDecline={offerId => {
        console.log('Declined ' + offerId)
      }}
      onComplete={offerId => {
        console.log('Completed ' + offerId)
      }}
      onReport={offerId => {
        console.log('Reported' + offerId)
      }}
      offerId="33kj3jk23d"
      status="pending"
    />
  ))
