import React, { Component } from 'react'
import { Row, Col, Card, Button } from 'antd'

interface State {
  isOpen: boolean
}

interface Props {
  HiddenSection: any
  MainSection: any
  // HiddenSectionProps: {}
  // MainSectionProps: {}
  style?: React.CSSProperties
}

class RevealCard<
  MainSectionPropsType extends object,
  HiddenSectionPropsType extends object
> extends Component<
  Props & { HiddenSectionProps: HiddenSectionPropsType; MainSectionProps: MainSectionPropsType },
  State
> {
  constructor(
    props: Props & {
      HiddenSectionProps: HiddenSectionPropsType
      MainSectionProps: MainSectionPropsType
    }
  ) {
    super(props)
    this.state = {
      isOpen: false
    }

    this.toggleOpen = this.toggleOpen.bind(this)
  }

  private toggleOpen() {
    this.setState((state, props) => {
      return { isOpen: !state.isOpen }
    })
  }

  public render() {
    const { MainSection, HiddenSection, MainSectionProps, HiddenSectionProps, style } = this.props
    const divStopPropagation = (e: any) => {
      e.stopPropagation()
    }

    return (
      <Card
        onClick={this.toggleOpen}
        hoverable={true}
        style={{ marginBottom: 10, borderRadius: 10, ...style }}>
        <Row type="flex" justify="space-between" align="middle">
          <MainSection {...MainSectionProps} />
          <Col span={2}>
            {/* <Button type='' onClick={this.toggleOpen} shape='circle' icon={this.state.isOpen ? 'up' : 'down'}></Button> */}
            <Button onClick={divStopPropagation} size="large" shape="circle" icon="message" />
          </Col>
        </Row>
        <div style={{ display: this.state.isOpen ? '' : 'none' }} onClick={divStopPropagation}>
          <HiddenSection {...HiddenSectionProps} />
        </div>
      </Card>
    )
  }
}

export default RevealCard
