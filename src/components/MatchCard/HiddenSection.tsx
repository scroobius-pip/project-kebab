import React from 'react'
import GameList from '../GameList'
import GameCheckBox from '../GameCheckBox'
import Container from '../Container'
import { Row, Col } from 'antd'
import ReactMarkdown from 'react-markdown'
import { GameCheckboxProps } from 'components/GameCheckBox/GameCheckBox'
import TaskButton from '../TaskButton'

export interface HiddenSectionProps {
  description: string
  onCheckGamesMe: (item: any) => any
  onCheckGamesOther: (item: any) => any
  onSubmit: () => any
  submitDisabled: () => boolean
  // loading: boolean
  userName: string
  gameListOther: USER_GAME[]
  gameListMe: USER_GAME[]
}

export default ({
  // loading,
  onCheckGamesMe,
  onCheckGamesOther,
  onSubmit,
  submitDisabled,
  userName,
  gameListOther,
  gameListMe,
  description
}: HiddenSectionProps) => {
  return (
    <div>
      <Container style={{ marginTop: 10 }}>
        <h3>Description</h3>
        <ReactMarkdown source={description} />
      </Container>
      <Row style={{ marginTop: 10 }} gutter={16}>
        <Col span={12}>
          <GameList<{ onChange: GameCheckboxProps['onChange'] }>
            style={{ maxHeight: 600 }}
            data={gameListMe}
            ListItem={GameCheckBox}
            listItemProps={{ onChange: onCheckGamesMe }}
            header={
              <div>
                <span>Me</span>
                <h1>I Will Give You:</h1>
              </div>
            }
          />
        </Col>
        <Col span={12}>
          <GameList<{ onChange: GameCheckboxProps['onChange'] }>
            style={{ maxHeight: 600 }}
            data={gameListOther}
            ListItem={GameCheckBox}
            listItemProps={{ onChange: onCheckGamesOther }}
            header={
              <div>
                <span>{userName}</span>
                <h1>I Want:</h1>
              </div>
            }
          />
        </Col>
      </Row>
      <Row type="flex" justify="end">
        <TaskButton
          style={{ marginTop: 20 }}
          disabled={submitDisabled()}
          normalText="Submit Offer"
          loadingText="Submitting"
          onClick={onSubmit}
          type="primary"
          size="large"
        />
      </Row>
    </div>
  )
}
