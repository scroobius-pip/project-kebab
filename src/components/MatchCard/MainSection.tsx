import React, { Fragment } from 'react'
import { Row, Col, Icon } from 'antd'
import ProfileImage from '../ProfileImage'

export interface MainSectionProps extends USER_TYPE {
  matchRate: number
  distance: number
  type: MATCH_TYPE['type']
}

export default ({
  id,
  matchRate,
  distance,
  userName,
  userImageUrl,
  isPro,
  joinedDate,
  noOfSuccessfulExchanges,
  rating,
  location: { state, city },
  type
}: MainSectionProps) => {
  return (
    <Fragment>
      <Col span={6}>
        <Row>
          <Col>
            <h1 style={{ marginBottom: 0, fontSize: 40 }}>
              {type === 'matchRate' ? `${matchRate}%` : distance ? `${distance}` : null}
            </h1>
          </Col>
          <Col>
            <span>{type === 'matchRate' ? 'Match Rate' : 'Distance (Km)'}</span>
          </Col>
        </Row>
      </Col>
      <Col span={16} offset={0}>
        <Row>
          <Col style={{ userSelect: 'none', textAlign: 'right' }} span={14}>
            <Col>
              <Icon />
              <span>{`${city}, ${state}`}</span>
            </Col>
            <Col>
              <h2>{userName}</h2>
            </Col>
          </Col>

          <Col span={5} offset={2}>
            <ProfileImage
              id={id}
              isPro={isPro}
              userImageUrl={userImageUrl}
              joinedDate={joinedDate}
              noOfSuccessfulExchanges={noOfSuccessfulExchanges}
              rating={rating}
            />
          </Col>
        </Row>
      </Col>
    </Fragment>
  )
}
