import React from 'react'
import { storiesOf } from '@storybook/react'

import MatchCard from './'

// const gameListMe: USER_GAME[] = [
//   {
//     name: 'Red Dead Redemption 2',
//     consoleType: 'PlayStation 4',
//     id: '1',
//     tradeType: 'swap',
//     description: 'About red dead www.google.com'
//   },
//   {
//     name: 'Spider Man',
//     consoleType: 'PlayStation 4',
//     id: '2',
//     tradeType: 'sale',
//     description: ''
//   },
//   {
//     name: 'Battlefield 2',
//     consoleType: 'PlayStation 3',
//     id: '3',
//     tradeType: 'swap',
//     description: ''
//   },
//   {
//     name: "Uncharted 3: Drake's Deception",
//     consoleType: 'PlayStation 3',
//     id: '4',
//     tradeType: 'swap',
//     description: ''
//   },
//   {
//     name: 'Fifa 16',
//     consoleType: 'PlayStation 3',
//     id: '5',
//     tradeType: 'swap',
//     description: ''
//   }
// ]

// const meInfo: USER_TYPE = {
//   rating: {
//     positive: 10,
//     negative: 0
//   },
//   id: '1',
//   description: 'About me',
//   isPro: true,
//   joinedDate: { month: 'December', year: '2014' },
//   noOfSuccessfulExchanges: 30,
//   userImageUrl: 'https://avatars3.githubusercontent.com/u/11554364?s=460&v=4',
//   location: { state: 'Fct', city: 'Abuja' },
//   userName: 'Sim04ful'
// }

const me: USER_WITH_GAMES = {
  info: {
    id: 'Endymion86',
    description:
      'Especially looking for Kingdom Hearts 3 & Soul Calibur VI. Willing to trade multiple titles depending on the outcome.\n\n[All of my games are in perfect condition, complete in box](https://imgur.com/0lcYgR7). Feel free to ask for pictures. Plenty of positive trades on here. Trades only, physical copies only. Plenty of confirmed trades on here.',
    userName: 'Parabolic Palindrome',
    isPro: true,
    joinedDate: {
      month: 'April',
      year: '2011'
    },
    location: {
      city: '',
      state: ''
    },
    noOfSuccessfulExchanges: 20,
    rating: {
      negative: 2,
      positive: 18
    },
    userImageUrl:
      'https://styles.redditmedia.com/t5_a3sv1/styles/profileIcon_zjdz1826ysh21.jpg?width=256&height=256&crop=256:256,smart&s=4fab44d9bf4d6a1708244b80c88bf9cf733895ab'
  },
  games: [
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Spider-Man\t',
      id: 'Spider-Man\t',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Red Dead Redemption 2\t',
      id: 'Red Dead Redemption 2\t',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Detroit: Become Human\t',
      id: 'Detroit: Become Human\t',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Monster Hunter: World\t',
      id: 'Monster Hunter: World',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Bioshock: The Collection\t',
      id: 'Bioshock: The Collection\t',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: "Dragon's Dogma: Dark Arisen\t",
      id: "Dragon's Dogma: Dark Arisen\t",
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Uncharted: The Nathan Drake Collection\t',
      id: 'Uncharted: The Nathan Drake Collection\t',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Final Fantasy XV\t',
      id: 'Final Fantasy XV\t',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Borderlands: The Handsome Collection\t',
      id: 'Borderlands: The Handsome Collection\t',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Mass Effect: Andromeda - Steelbook Edition\t',
      id: 'Mass Effect: Andromeda - Steelbook Edition\t',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    }
  ]
}
// const gameListOther: USER_GAME[] = [
//   {
//     name: 'Rise of the Tomb Raider',
//     consoleType: 'PlayStation 4',
//     id: '7',
//     tradeType: 'sale',
//     description: ''
//   },
//   {
//     name: 'Driveclub',
//     consoleType: 'PlayStation 4',
//     id: '8',
//     tradeType: 'swap',
//     description: ''
//   },
//   {
//     name: 'Monster Hunter: World',
//     consoleType: 'PlayStation 4',
//     id: '9',
//     tradeType: 'swap',
//     description: ''
//   },
//   {
//     name: "Assassin's Creed Odyssey",
//     consoleType: 'PlayStation 4',
//     id: '10',
//     tradeType: 'swap',
//     description: ''
//   }
// ]

// const otherInfo: USER_TYPE = {
//   rating: {
//     positive: 10,
//     negative: 22
//   },
//   id: '2',
//   description:
//     "Hello! I'm almost exclusively looking for NIB Zelda games and NIB/CIB Famicom/Super Famicom stuff. Not that I won't entertain any other offers but those are going to be my primary focus. I'm trying to get my brother something nice as a housewarming presen\n\nt.\n\n[https://imgur.com/gallery/sa5K5Jq](https://imgur.com/gallery/sa5K5Jq)\n",
//   isPro: false,
//   joinedDate: { month: 'July', year: '2017' },
//   noOfSuccessfulExchanges: 3,
//   userImageUrl: 'https://avatars3.githubusercontent.com/u/11554364?s=460&v=4',
//   location: { state: 'Fct', city: 'Abuja' },
//   userName: 'Xxtentacles'
// }

const other: USER_WITH_GAMES = {
  info: {
    id: 'simdi',
    description:
      'Especially looking for Kingdom Hearts 3 & Soul Calibur VI. Willing to trade multiple titles depending on the outcome.\n\n[All of my games are in perfect condition, complete in box](https://imgur.com/0lcYgR7). Feel free to ask for pictures. Plenty of positive trades on here. Trades only, physical copies only. Plenty of confirmed trades on here.',
    userName: 'sim04ful',
    isPro: true,
    joinedDate: {
      month: 'April',
      year: '2016'
    },
    location: {
      city: 'Usa',
      state: 'Michigan'
    },
    noOfSuccessfulExchanges: 10,
    rating: {
      negative: 2,
      positive: 8
    },
    userImageUrl: 'https://www.redditstatic.com/avatars/avatar_default_08_FF66AC.png'
  },
  games: [
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Soul Calibur VI',
      id: 'Soul Calibur VI',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Anthem',
      id: 'Anthem',
      description: 'CIB',
      consoleType: 'PlayStation 4'
    },
    {
      tradeType: 'swap',
      imageUrl: '',
      name: 'Super Smash Bros. Ultimate',
      id: 'Super Smash Bros. Ultimate',
      description: 'CIB',
      consoleType: 'Switch'
    }
  ]
}

storiesOf('Match Card', module)
  .add('With Match Rate', () => (
    <MatchCard
      type="matchRate"
      me={me}
      other={other}
      distance={20.5}
      matchRate={75}
      onSubmit={submission => {
        console.log(submission)
      }}
    />
  ))
  .add('With Distance', () => (
    <MatchCard
      type="distance"
      me={me}
      other={other}
      matchRate={75}
      distance={20.5}
      onSubmit={submission => {
        console.log(submission)
      }}
    />
  ))
