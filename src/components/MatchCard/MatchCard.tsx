import React, { PureComponent } from 'react'
import RevealCard from '../RevealCard'
import HiddenSection, { HiddenSectionProps } from './HiddenSection'
import MainSection, { MainSectionProps } from './MainSection'

type USER_GAME_SELECTOR = Array<USER_GAME & { selected: boolean }>

interface State {
  // isLoading: boolean
  gamesMe: USER_GAME_SELECTOR
  gamesOther: USER_GAME_SELECTOR
}

interface Props {
  matchRate: number
  distance: number
  type: MATCH_TYPE['type']
  me: USER_WITH_GAMES
  other: USER_WITH_GAMES
  onSubmit: (submission: OFFER_SUBMISSION) => any
}

export default class extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props)

    const gamesCheckedMe = this.props.me.games.map(game => {
      return { ...game, selected: false }
    })

    const gamesCheckedOther = this.props.other.games.map(game => {
      return { ...game, selected: false }
    })

    this.state = {
      // isLoading: false,
      gamesMe: gamesCheckedMe,
      gamesOther: gamesCheckedOther
    }
  }

  private submitSubmission = () => {
    const { gamesMe, gamesOther } = this.state
    const myGamesChecked = gamesMe.filter(x => x.selected)
    const otherGamesChecked = gamesOther.filter(x => x.selected)

    this.props.onSubmit({
      otherId: this.props.other.info.id,
      meId: this.props.me.info.id,
      meSelectedGames: myGamesChecked,
      otherSelectedGames: otherGamesChecked
    })
  }

  private toggleGameMe = (gameId: string) => {
    this.setState(prevState => {
      const { gamesMe } = prevState
      const gameToggled = gamesMe.map(game => {
        if (game.id === gameId) {
          return { ...game, selected: !game.selected }
        }
        return game
      })

      return { gamesMe: gameToggled }
    })
  }

  private toggleGameOther = (gameId: string) => {
    this.setState(prevState => {
      const { gamesOther } = prevState
      const gameToggled = gamesOther.map(game => {
        if (game.id === gameId) {
          return { ...game, selected: !game.selected }
        }
        return game
      })
      return { gamesOther: gameToggled }
    })
  }

  private meSelectedAndOtherSelected = () => {
    const { gamesMe, gamesOther } = this.state
    const noMyGamesChecked = gamesMe.filter(x => x.selected).length
    const noOtherGamesChecked = gamesOther.filter(x => x.selected).length
    return noMyGamesChecked < 1 || noOtherGamesChecked < 1
  }

  public render() {
    const {
      me: { games: gameListMe },
      other: {
        info: {
          userName,
          userImageUrl,
          noOfSuccessfulExchanges,
          joinedDate,
          isPro,
          rating,
          location,
          description,
          id
        },
        games: gameListOther
      },
      matchRate,
      distance,
      type
    } = this.props
    return (
      <RevealCard<MainSectionProps, HiddenSectionProps>
        style={{}}
        MainSection={MainSection} // always visible part of the matchcard
        HiddenSection={HiddenSection} // togglable part of the matchcard
        MainSectionProps={{
          description,
          id,
          matchRate,
          location,
          userName,
          userImageUrl,
          distance,
          noOfSuccessfulExchanges,
          joinedDate,
          isPro,
          rating,
          type
        }}
        HiddenSectionProps={{
          userName, // username of other user
          description,
          onCheckGamesMe: this.toggleGameMe,
          onCheckGamesOther: this.toggleGameOther,
          // loading: this.state.isLoading,
          onSubmit: this.submitSubmission,
          submitDisabled: this.meSelectedAndOtherSelected,
          gameListMe,
          gameListOther
        }}
      />
    )
  }
}

// export default MatchCard
