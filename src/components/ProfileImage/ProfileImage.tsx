import React from 'react'
import { Popover, Avatar, Tag } from 'antd'

export default ({
  noOfSuccessfulExchanges,
  joinedDate: { month, year },
  userImageUrl,
  isPro,
  rating,
  size
}: Pick<USER_TYPE, Exclude<keyof USER_TYPE, 'location' | 'userName' | 'description'>> & {
  size?: number
}) => (
  <Popover
    content={
      <div style={{ padding: 5, textAlign: 'center' }}>
        <h3>
          (<span style={{ color: 'green' }}>+{rating.positive}</span>/
          <span style={{ color: 'red' }}>-{rating.negative}</span>)
        </h3>
        <p>
          Trades: <b>{noOfSuccessfulExchanges}</b>
        </p>
        <p>
          Joined:{' '}
          <b>
            {month}, {year}
          </b>
        </p>
        {isPro ? <Tag color="purple">Pro Member</Tag> : null}
      </div>
    }>
    <Avatar size={size || 50} src={userImageUrl} />
  </Popover>
)
