import React from 'react'
import { storiesOf } from '@storybook/react'

import DescriptionBox from './'
import 'antd/dist/antd.css'

storiesOf('Description Box', module).add('Normal', () => (
  <DescriptionBox
    getDescription={async () => ''}
    onChange={description => {
      console.log(description)
    }}

  />
))
