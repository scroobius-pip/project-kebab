import React, { PureComponent } from 'react'
import LzEditor from 'react-lz-editor'
import { Card } from 'antd'
import WithSpin from '../WithSpin';

interface State {
  markdownContent: string
  responseList: string
  loading: boolean
}

interface Props {
  onChange: (markdownContent: string) => any
  getDescription: () => Promise<string>
}

class DescriptionBox extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      markdownContent: '',
      responseList: '',
      loading: true
    }
  }
  public async componentDidMount() {
    this.setState({
      loading: true
    })

    const description = await this.props.getDescription()

    this.setState({
      markdownContent: description,
      loading: false
    })
  }

  public componentDidUpdate(prevProps: Props, prevState: State) {
    this.props.onChange(this.state.responseList)
  }

  private receiveMarkdown = (content: any) => {
    this.setState({ responseList: content })
  }

  public render() {
    return (
      <WithSpin loading={this.state.loading}>
        <Card
          style={{
            borderRadius: 5
          }}>
          <div
            style={{
              padding: 20
            }}>
            <h2>Description</h2>

            <LzEditor
              autoSave={false}
              color={false}
              active={true}
              image={false}
              video={false}
              audio={false}
              convertFormat="markdown"
              cbReceiver={this.receiveMarkdown}
              importContent={this.state.markdownContent}
            />
          </div>
        </Card>
      </WithSpin>
    )
  }
}

export default DescriptionBox
