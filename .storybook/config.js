import { configure } from '@storybook/react'
import '@storybook/addon-knobs/register'
import '@storybook/addon-console'

const req = require.context('../src/components', true, /\.story\.tsx$/)

function loadStories() {
  // require('../stories/index.tsx');
  // You can require as many stories as you need.
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
