const path = require('path')
const { getLoader, injectBabelPlugin } = require('react-app-rewired')
const tsImportPluginFactory = require('ts-import-plugin')

// const TSDocgenPlugin = require("react-docgen-typescript-webpack-plugin");
module.exports = (baseConfig, env, config) => {
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    loader: require.resolve('awesome-typescript-loader')
  })

  //   config.plugins.push(new TSDocgenPlugin()); // optional
  config.resolve.extensions.push('.ts', '.tsx')

  config = injectBabelPlugin(
    ['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }],
    config
  )

  // Make whatever fine-grained changes you need

  config.module.rules.push({
    test: /\.less$/,
    // loaders: ['style-loader', 'css-loader', 'less-loader'],
    use: [
      { loader: 'style-loader' },
      { loader: 'css-loader' },
      {
        loader: 'less-loader',
        options: {
          javascriptEnabled: true
        }
      }
    ]
    // include: path.resolve(__dirname, '../src/')
  })
  // config.module.rules.push({
  //     test: /\.css$/,
  //     loaders: ['css-loader', 'style-loader', 'less-loader'],
  //     // include: path.resolve(__dirname, '../src/')
  // });
  return config
}
